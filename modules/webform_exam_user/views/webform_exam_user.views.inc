<?php
/**
  * Implements hook_views_data
  */
function webform_exam_user_views_data() {
  $data = array();
  $data['webform_exam_user']['table']['group'] = t('User');
  $data['webform_exam_user']['table']['base'] = array(
    'field' => 'uid', 
    'title' => t('Webform exam user table'), 
    'help' => t("Webform exam user table contains Webform exam user content and can be related to users."), 
    'weight' => -10,
  );
  $data['webform_exam_user']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
      'type' => 'LEFT',
    ),
  );
  $data['webform_exam_user']['publish'] = array(
    'title' => t('Webform Public'), 
    'help' => t('Webform examination certifications are public.'), 
    'field' => array(
      'handler' => 'views_handler_field_boolean', 
      'click sortable' => TRUE,
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Published'),
      'type' => 'yes-no',
      'accept null' => TRUE,
      'use equal' => TRUE, // Use status = 1 instead of status <> 0 in WHERE statment
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}
