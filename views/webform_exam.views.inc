<?php
/**
  * Implements hook_views_data
  */
function webform_exam_views_data() {
  $data = array();
  $data['webform_exam_submissions']['table']['group'] = t('Webform submissions');
  $data['webform_exam_submissions']['table']['base'] = array(
    'field' => 'sid', 
    'title' => t('Webform exam user table'), 
    'help' => t("Webform exam user table contains Webform exam user content and can be related to users."), 
    'weight' => -10,
  );
  $data['webform_exam_submissions']['table']['join'] = array(
    'webform_submissions' => array(
      'left_field' => 'sid',
      'field' => 'sid',
      'type' => 'LEFT',
    ),
  );
/*
  $data['webform_exam_submissions']['sid'] = array(
    'title' => t('Sid'),
    'help' => t('The submission ID of the submission.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'title' => t('Sid'),
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
*/
  $data['webform_exam_submissions']['nid'] = array(
    'title' => t('Node'),
    'help' => t('The webform node this submission was generated from.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Webform Node'),
    ),
  );
  $data['webform_exam_submissions']['state_changed'] = array(
    'title' => t('Changed'),
    'help' => t('The date this submission state was changed.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'title' => t('Changed'),
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );
  $data['webform_exam_submissions']['state'] = array(
    'title' => t('State'),
    'help' => t('The examination state of this submission.'),
    'field' => array(
      'handler' => 'webform_exam_views_handler_field_state',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'title' => t('State'),
      'handler' => 'webform_exam_views_handler_filter_state',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['webform_exam_submissions']['data'] = array(
    'title' => t('Data'),
    'help' => t('Additional examination webform submission data.'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
      'click sortable' => TRUE,
    ),
  );
  return $data;
}
