<?php
/**
 * @file
 * Definition of views_handler_field_boolean.
 */

/**
 * A handler to provide proper displays for booleans.
 *
 * Allows for display of true/false, yes/no, on/off, enabled/disabled.
 *
 * Definition terms:
 *   - output formats: An array where the first entry is displayed on boolean true
 *      and the second is displayed on boolean false. An example for sticky is:
 *      @code
 *      'output formats' => array(
 *        'sticky' => array(t('Sticky'), ''),
 *      ),
 *      @endcode
 *
 * @ingroup views_field_handlers
 */
class webform_exam_views_handler_field_state extends views_handler_field {

  function render($values) {
    $value = $this->get_value($values);
    switch ($value) {
      case WEBFORM_GRADE_ANSWER:
        return t('Answer');
      case WEBFORM_GRADE_REVIEW:
        return t('Review');
      case WEBFORM_GRADE_PASSED:
        return t('Passed');
    }
  }

}