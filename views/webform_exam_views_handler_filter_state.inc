<?php

/**
 * @file
 * Definition of webform_exam_views_handler_filter_state.
 */

/**
 * Filter by webform examination state.
 *
 * @ingroup views_filter_handlers
 */
class webform_exam_views_handler_filter_state extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('States');
      $types = array(
        WEBFORM_GRADE_ANSWER => array('name' => t('Answer')),
        WEBFORM_GRADE_REVIEW => array('name' => t('Review')),
        WEBFORM_GRADE_PASSED => array('name' => t('Passed')),
      );
      $options = array();
      foreach ($types as $type => $info) {
        $options[$type] = t($info['name']);
      }
      $this->value_options = $options;
    }
  }
}

